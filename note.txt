- For create new app laravel with specific versio:
>>composer global require laravel/installer
>>composer create-project --prefer-dist laravel/laravel laravle-api-2 "10.*"

-Create new app laravel with the newest version:
>>laravel new <project_name>

****************-----------------IMPORTANT CONCEPT IN LARAVEL------------------********************
1. Devine databse configuration in env 
2. Create model-model yang ingin dibuat :
    >> php artisan make:model <NamaModel : Customer> --all
    >> php artisan make:model <NamaModel : Invoice> --all
3. After create model-model kalian bisa menambahkan reals antar model:
contoh, pada file Invoice.php yang terdapat folder Models di dalam folder app

class Invoice extends Model{
    use HasFactory;
    public function customers(){
        return $this->belongsTo(Customer::class)
    }
}

4. Pilih file migration atas model yang telah dibuat dengan cara buka folder migrations yang terdapat pada folder database untuk menambahkan
   kolom-kolom yang dibutuhkan padal model(table) terkait contoh pada model invoice:

   ...
    Schema::create('invoices', function(Blueprint $table){
        $table->id();
        $table->integer('customer_id');
        $table->string('status');
        $table->dateTime('Paid_dated)->nullable
    })
   ..

5. Kalian juga bisa menambhakan initial data menggunakan factory dan seeder
6. Lakukan migration ke database:
>> php artisan migrate:refresh --seed

7. Lakukan pembuatan routing (bisa dengan berbagai cara) :
- pertama buka controller file model yang ingin dibuat routingnya contoh customer,
anda bisa menambhakan code ini untuk membuat route yang akan dibuat mengembalikan seluruh data yang ada di table
customer :

...
    public function index()
    {
        return Invoice::all();

    }
...

-. Kemudian definisikan routing untuk customer pada file api.php:

bisa dengan code berikut jika anda melakukan versioning pada api dan menggunkan resources

Route::group(['prefix' => 'v1', 'namespace' => 'App\Http\Controllers\Api\V1'], function () {
    Route::apiResource('customers', CustomerController::class);
});

8. Kita bisa mengatur apa saja data yang akan kita return dari api yang telah kita buat dengan cara
membuat resources:
>>php artisan make:resource V1\CustomerResource

Resource classes in Laravel are responsible for transforming models and collections into JSON responses. 
They provide a convenient and consistent way to format the output of your API responses.


9. Pagination dapat dilakukan pada file controller atas modul yang igin di paginate:
contoh code;
...
    public function index()
    {
        return new V1CustomerCollection(Customer::paginate());
    }

    cara merubah pagination dapat dilakukan dengan, menambahkan search param page pada route yang telah dibuat:
    http://127.0.0.1:8000/api/v1/customers?page=10

...



10. Untuk membuat new request gunakan perintah:
>>php artisan make:request StoreCustomerRequest

11. Tentang token pada laravel:
kita dapat meng-asign ability pada token jika kita tidak meng-assign ability pada token 
maka token dapat memiliki seluruh akses


12. Untuk menginstall new dependecies pada laravel gunakan, contoh:
>>composer require laravel/sanctum